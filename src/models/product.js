const mongoose = require('mongoose');


const Product = mongoose.model('Product', {
	nombre: {
		type: String,
		required: true,
        trim: true
	},

	descripcion: {
		type: String,
		required: false,
        trim: false
	}
})

module.exports = Product