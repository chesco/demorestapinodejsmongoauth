# Demo Rest Api con Auth y mongoDB

Demo de un rest api para crear productos en mongoDB y que posee autenticación

## Comenzando 🚀

Clonas el proyecto de bitbucket
git clone https://chesco@bitbucket.org/chesco/demorestapinodejsmongoauth.git


### Pre-requisitos 📋

Nodejs
Postman


### Instalación 🔧

Se debe tener instalado nodejs

Se realiza la instalación de las dependencias con el comamdo

``` bash
	npm install 
```
Ya tiene una base de dato por defecto , pero si deseas puedes cambiar los datos, dentro del archivo ./src/db/mongoose.js

Se levanta el servidor localmente

``` bash
	node ./src/index.js
```
Este api consta de 9 servicios los cuales son

## POST http://127.0.0.1:3000/users/register

Descripción: Servicio que permite el registro de un usuario, el "username" debe ser único, no requiere autorización
``` bash
Body
{
  "username": "Nombre",
  "password": "12345236"
}

Response
{
    "user": {
        "_id": "5c8aca4401a4f6bce88ee315",
        "username": "Nombre",
        "password": "$2a$08$MJAFqFZypY186cM5hMbUQ.q19fQ4wWLRS8YGxTZDYzpGf2lWKe6O2",
        "tokens": [
            {
                "_id": "5c8aca4401a4f6bce88ee316",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhhY2E0NDAxYTRmNmJjZTg4ZWUzMTUiLCJpYXQiOjE1NTI1OTk2MjB9.IhN_U901ZWeFM_xFfpeIZpCKmu6YbZR_ZlWj6-hjmgI"
            }
        ],
        "__v": 1
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhhY2E0NDAxYTRmNmJjZTg4ZWUzMTUiLCJpYXQiOjE1NTI1OTk2MjB9.IhN_U901ZWeFM_xFfpeIZpCKmu6YbZR_ZlWj6-hjmgI"
}
``` 

## POST http://127.0.0.1:3000/users/login

Descripción: Servicio que permite a un usuario registrado conseguir un toke, no requiere autorización
``` bash
Body 

{
  "username": "Nombre",
  "password": "12345236"
}

Response
{
    "user": {
        "_id": "5c8aca4401a4f6bce88ee315",
        "username": "Nombre",
        "password": "$2a$08$MJAFqFZypY186cM5hMbUQ.q19fQ4wWLRS8YGxTZDYzpGf2lWKe6O2",
        "tokens": [
            {
                "_id": "5c8aca4401a4f6bce88ee316",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhhY2E0NDAxYTRmNmJjZTg4ZWUzMTUiLCJpYXQiOjE1NTI1OTk2MjB9.IhN_U901ZWeFM_xFfpeIZpCKmu6YbZR_ZlWj6-hjmgI"
            }
        ],
        "__v": 1
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhhY2E0NDAxYTRmNmJjZTg4ZWUzMTUiLCJpYXQiOjE1NTI1OTk2MjB9.IhN_U901ZWeFM_xFfpeIZpCKmu6YbZR_ZlWj6-hjmgI"
}
```
## GET http://127.0.0.1:3000/users
Descripción: Se obtienen todos los usuarios registrados, requiere autorización
```
Header
Key "Authorization" value ="Bearer <Token obtenido al hacer login o registrarse>" 

response
[
    {
        "_id": "5c8ad1c33c1606bd39a5c551",
        "username": "chescodlb",
        "password": "$2a$08$XF9UGmFkQ/TFdpf3X/qzluMcwtS07ly0g9.g1sRj2WJFFNfSHV3me",
        "tokens": [
            {
                "_id": "5c8ad1c43c1606bd39a5c552",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhhZDFjMzNjMTYwNmJkMzlhNWM1NTEiLCJpYXQiOjE1NTI2MDE1NDB9.43pqpyXNH9zBsRrcJNjRmY7dAzlHd1Fct52DsViL4qE"
            }
        ],
        "__v": 1
    },
    {
        "_id": "5c8ad1dc3c1606bd39a5c553",
        "username": "NameER·SDF",
        "password": "$2a$08$pPVd1JR2tZvvhgvBejEV5OmMrWOUcU8bELJgmeZpX6Oos41rwOEPW",
        "tokens": [
            {
                "_id": "5c8ad1dc3c1606bd39a5c554",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhhZDFkYzNjMTYwNmJkMzlhNWM1NTMiLCJpYXQiOjE1NTI2MDE1NjR9.xRmwz3fzCIgOMLIMepipQPDpE8Di0oT8roCgCE_dNqE"
            },
            {
                "_id": "5c8ad2043c1606bd39a5c555",
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhhZDFkYzNjMTYwNmJkMzlhNWM1NTMiLCJpYXQiOjE1NTI2MDE2MDR9.uCOT5UaLrQqDtoJPnrBftgZ0E1ovishwSLTeXkJ_xbs"
            }
        ],
        "__v": 2
    }
]
```
## GET http://127.0.0.1:3000/users/:id
Descripción: Obtiene un resultado si el id de usuario se encuantra registrado, requiere autorización

```
Path Variables 
Key "id" Value "<Id de usuario>"

Header
Key "Authorization" value ="Bearer <Token obtenido al hacer login o registrarse>" 


response
{
    "_id": "5c8ad1c33c1606bd39a5c551",
    "username": "chescodlb",
    "password": "$2a$08$XF9UGmFkQ/TFdpf3X/qzluMcwtS07ly0g9.g1sRj2WJFFNfSHV3me",
    "tokens": [
        {
            "_id": "5c8ad1c43c1606bd39a5c552",
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhhZDFjMzNjMTYwNmJkMzlhNWM1NTEiLCJpYXQiOjE1NTI2MDE1NDB9.43pqpyXNH9zBsRrcJNjRmY7dAzlHd1Fct52DsViL4qE"
        }
    ],
    "__v": 1
}
```

## post http://127.0.0.1:3000/products
Descripción: Obtiene un resultado si el id de usuario se encuantra registrado, requiere autorización

```
Header
Key "Authorization" value ="Bearer <Token obtenido al hacer login o registrarse>" 

request body
{
	"nombre":"<NombreProducto>",
	"descripcion":"<DescripcionProducto>"
}

response
{
    {
    "_id": "5c8ad4af3c1606bd39a5c556",
    "nombre": "<NombreProducto>",
    "descripcion": "<DescripcionProducto>",
    "__v": 0
}
}
```

## GET http://127.0.0.1:3000/product/:id
Descripción: Obtiene un resultado si el id de usuario se encuantra registrado, requiere autorización

```
Path Variables 
Key "id" Value "<Id de producto>"

Header
Key "Authorization" value ="Bearer <Token obtenido al hacer login o registrarse>" 

request body


response
{
    "_id": "5c8ad4af3c1606bd39a5c556",
    "nombre": "<NombreProducto>",
    "descripcion": "<DescripcionProducto>",
    "__v": 0
}
```

## DELETE http://127.0.0.1:3000/users/:id
Descripción: Obtiene un resultado si el id de usuario se encuantra registrado, requiere autorización

```
Path Variables 
Key "id" Value "<Id de producto>"

Header
Key "Authorization" value ="Bearer <Token obtenido al hacer login o registrarse>" 


response
{
    "_id": "<Id de producto>",
    "nombre": "<NombreProducto>",
    "descripcion": "<DescripcionProducto>",
    "__v": 0
}
```

## Construido con 🛠️

* Node.js
* MongoDB
* Mongoose
* Express
* Javascript


## Autores ✒️

Francisco J. De La Blanca

